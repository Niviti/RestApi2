from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status 
from rest_framework import viewsets
from . import serializer


class HelloApiView(APIView):
    """Test API View."""
    
    serializer_class = serializer.HelloSerializer   

    def get(self, request, format=None):
        """Returns a list of APIview features."""

        an_apiview = [
            'Uses HTTP methods as function (get, post , patch, put, delete)',
            'Gives you most control over your logic',
            'It is similar to a traditional Django view',
            'Is Mapped Manulay to URLS'
        ]

        return Response({'message': 'Hello!', 'an_apiview': an_apiview})  

    def post(self, request):
        """Create a hello message with our name."""

        serializers = serializer.HelloSerializer(data=request.data)
        
        if serializers.is_valid():
             name = serializers.data.get('name')
             message = 'Hello {0}'.format(name)
             return Response({'message': message})
        else:
            return Response(
                   serializers.errors, status=status.HTTP_400_BAD_REQUEST)     


    def put(self, request, pk=None):
        """ Hanles updating an object."""             

        return Response({'method': 'put'})  

    def patch(self, request, pk=None):
        """ updatejtuje tylko te czesc ktora wysyłamy """

        return Response({'method': 'patch'})

    def delete(self, request, pk=None):
        """Deletes and ojbec."""

        return Response({'method': 'delete'})
 

class HelloViewSet(viewsets.ViewSet):
    """Test Api View Set"""

    def list(self,request):
           """Return Hello message."""
           
           serializer_class = serializer.HelloSerializer 

           a_viewset = [
            'Uses actions (list,create, retrive, update, partical_update)',
            'Automatically maps to URLS using Routers',
            'Provides more functionality with less code.'
           ]



           return Response({'message': 'Hello!', 'a_viewset': a_viewset })

    def create(self, request):
        """Create a new hello message.""" 

        serializers = serializer.HelloSerializer(data=request.data) 

        if serializers.is_valid():
              name = serializers.data.get('name')
              message = 'Hello {0}'.format(name)
              return Response({'message': message})
        else:
             return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)     

    def retrive(self, request, pk=None):
        """Handles getting an object by its ID."""

        return Response({'http_method': 'GET'})

    def update(self, request, pk=None):
        """Handles updating an object."""

        return Response({'http_method:' 'PUT'})

    def partical_update(self, request, pk=None):
        """Handles updating part of an object."""

        return Response({'http_method': 'PATCH'})         

    def destroy(self, request, pk=None):
        """Handles removing an object."""

        return Response({'http_method': 'DELETE'})    

